﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment1b
{
    public class Booking
    {
        private Client client;
        private Appointment appointment;
        private Dog dog;

        public Booking()
        {
        }

        public Client Client
        {
            get { return client;  }
            set { client = value;  }
        }

        public Dog Dog
        {
            get { return dog; }
            set { dog = value; }
        }

        public Appointment Appointment
        {
            get { return appointment; }
            set { appointment = value; }
        }

        // Returns a double representing the total price of the booking
        public double CalculatePrice()
        {
            double price = 10.00;

            price += 5.00 * appointment.ServicesRequired.Count;
            if(appointment.GetDog)
            {
                price += 2.50;
            }
            if(appointment.ReturnDog)
            {
                price += 2.50;
            }

            price += 1.00 * (appointment.ReturnDate.Hour - appointment.ReceiveDate.Hour);

            return price;
        }

        // Returns a string containing information about how the price of the booking was calculated
        public string GetPricingSummary()
        {
            string pricingSummary = "<h2>Pricing Summary:</h2>";

            pricingSummary += "Base Price: $10.00 <br />";
            pricingSummary += "$5.00 per service (You have " + appointment.ServicesRequired.Count + " services) <br />";
            pricingSummary += "$2.50 to pick up your dog (You have requested this service: " + appointment.GetDog + ") <br />";
            pricingSummary += "$2.50 to drop off your dog (You have requested this service: " + appointment.ReturnDog + ") <br />";
            pricingSummary += "$1.00 for every hour of your appointment (Your appointment is "
                + (appointment.ReturnDate.Hour - appointment.ReceiveDate.Hour) + " hours long) <br />";

            return pricingSummary;
        }

        // Returns a string containing all the details of the booking
        public string GetBookingDetails()
        {
            string bookingDetails = "<h2>Booking details:</h2>";

            bookingDetails += "<fieldset><legend>Your Information</legend>";
            bookingDetails += "Your Name: " + client.clientName + "<br />";
            bookingDetails += "Your Phone Number: " + client.clientPhone + "<br />";
            bookingDetails += "Your Email Address: " + client.clientEmail + "<br />";
            bookingDetails += "Your Street Address: " + client.clientStreetAddress + "<br />";
            bookingDetails += "Your Postal Code: " + client.clientPostalCode + "<br />";
            bookingDetails += "Subscribe to Emails: " + client.clientWantsEmail.ToString() + "</fieldset>";

            bookingDetails += "<fieldset><legend>Your Dog's Information</legend>";
            bookingDetails += "Your Dog's Name: " + dog.DogName + "<br />";
            bookingDetails += "Your Dog's Breed: " + dog.DogBreed + "</fieldset>";

            bookingDetails += "<fieldset><legend>Services Details</legend>";
            bookingDetails += "Services Required: " + String.Join(", ", appointment.ServicesRequired.ToArray()) + "<br />";
            if(appointment.DogWalkLength != 0)
            {
                bookingDetails += "You requested to have your dog walked for " + appointment.DogWalkLength + " minutes <br />";
            }
            if(appointment.GetDog == true)
            {
                bookingDetails += "Start of Appointment: You requested to have your dog picked up from your address <br />";
            }
            else
            {
                bookingDetails += "Start of Appointment: You requested to come drop off your dog <br />";
            }
            if (appointment.ReturnDog == true)
            {
                bookingDetails += "End of Appointment: You requested to have your dog dropped off at your address <br />";
            }
            else
            {
                bookingDetails += "End of Appointment: You requested to come pick up your dog <br />";
            }
            bookingDetails += "Appointment Start Date: " + appointment.ReceiveDate.ToString() + "<br />";
            bookingDetails += "Appointment End Date: " + appointment.ReturnDate.ToString() + "</fieldset>";

            return bookingDetails;
        }
    }
}