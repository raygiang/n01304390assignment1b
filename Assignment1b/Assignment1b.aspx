﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Assignment1b.aspx.cs" Inherits="Assignment1b.Assignment1b" %>

<!DOCTYPE html>
<%-- Assignment 1b
     Raymond Giang
     n01304390 --%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Local Dog Services</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>Local Dog Services</h1>
            
            <asp:ValidationSummary id="validSummary" Runat="server" ForeColor="Red"/>

            <asp:label runat="server" AssociatedControlID="clientName">Client Name: </asp:label>
            <asp:TextBox runat="server" ID="clientName" placeholder="Your Name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Enter Your Name" ForeColor="Red" ControlToValidate="clientName" ID="nameValidator"></asp:RequiredFieldValidator>
            <br />

            <asp:label runat="server" AssociatedControlID="clientPhone">Phone Number: </asp:label>
            <asp:TextBox runat="server" ID="clientPhone" placeholder="e.g. 905-111-2222"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Enter a Phone Number" ForeColor="Red" ControlToValidate="clientPhone" ID="phoneValidator"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regexPhone" runat="server" ValidationExpression="^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$" ControlToValidate="clientPhone" ErrorMessage="Invalid Phone Number" ForeColor="Red"></asp:RegularExpressionValidator>
            <%// Phone Number Regular Expression is From: https://stackoverflow.com/questions/16699007/regular-expression-to-match-standard-10-digit-phone-number %>
            <br />

            <asp:Label runat="server" AssociatedControlID="clientEmail">Email Address: </asp:Label>
            <asp:TextBox runat="server" ID="clientEmail" placeholder="Your Email Address"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Enter Your Email Address" ForeColor="Red" ControlToValidate="clientEmail" ID="emailValidator"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regexEmail" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="clientEmail" ErrorMessage="Invalid Email Address Format" ForeColor="Red"></asp:RegularExpressionValidator>
            <%// Email Regular Expression From Lecture Server Controls Example %>
            <br />

            <asp:Label runat="server" AssociatedControlID="clientStreetAddress">Street Address: </asp:Label>
            <asp:TextBox runat="server" ID="clientStreetAddress" placeholder="Your Street Address"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Enter Your Street Address" ForeColor="Red" ControlToValidate="clientStreetAddress" ID="streetAddressValidator"></asp:RequiredFieldValidator>
            <asp:CompareValidator runat="server" ControlToValidate="clientStreetAddress" Type="String" Operator="NotEqual" ValueToCompare="123 Fake Street" ErrorMessage="This is not a Valid Street Address" ForeColor="Red" ID="realStreetValidator"></asp:CompareValidator>
            <br />
            
            <asp:Label runat="server" AssociatedControlID="clientPostalCode">Postal Code: </asp:Label>
            <asp:TextBox runat="server" ID="clientPostalCode" placeholder="Your Postal Code"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Enter Your Postal Code" ForeColor="Red" ControlToValidate="clientPostalCode" ID="postalValidator"></asp:RequiredFieldValidator>
            <br />
            <br />

            <asp:Label runat="server" AssociatedControlID="dogName">Dog Name: </asp:Label>
            <asp:TextBox runat="server" ID="dogName" placeholder="Your Dog's Name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Enter Your Dog's Name" ForeColor="Red" ControlToValidate="dogName" ID="dogNameValidator"></asp:RequiredFieldValidator>
            <br />

            <asp:Label runat="server" AssociatedControlID="dogBreed">Dog Breed: </asp:Label>
            <asp:TextBox runat="server" ID="dogBreed" placeholder="Your Dog's Breed"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Enter Your Dog's Breed" ForeColor="Red" ControlToValidate="dogBreed" ID="dogBreedValidator"></asp:RequiredFieldValidator>
            <br />

            <asp:Label runat="server" AssociatedControlID="appointmentServices">What services do you need?</asp:Label>
            <asp:CheckBoxList runat="server" ID="appointmentServices" RepeatDirection="Horizontal">
                <asp:ListItem Value="Walking">Dog Walking</asp:ListItem>
                <asp:ListItem Value="Bathing">Dog Bathing</asp:ListItem>
                <asp:ListItem Value="Sitting">Dog Sitting</asp:ListItem>
            </asp:CheckBoxList>
            <asp:CustomValidator runat="server" ErrorMessage="You Must Select At Least One Service" ForeColor="Red" ControlToValidate="appointmentWalkLength" OnServerValidate="servicesValidator" ValidateEmptyText="true"></asp:CustomValidator>
            <br />    

            <asp:Label runat="server" AssociatedControlID="appointmentWalkLength">How long would you like your dog walked? (if applicable): </asp:Label>
            <asp:TextBox runat="server" ID="appointmentWalkLength" placeholder="30 to 120 mins"></asp:TextBox>
            <asp:RangeValidator runat="server" ControlToValidate="appointmentWalkLength" Type="Integer" MinimumValue="30" MaximumValue="120" ErrorMessage="Enter a valid time (30 - 120)" ForeColor="Red"></asp:RangeValidator>
            <asp:CustomValidator runat="server" ErrorMessage="This field is required if you want your dog walked!" ForeColor="Red" ControlToValidate="appointmentWalkLength" OnServerValidate="appointmentWalkValidator" ValidateEmptyText="true"></asp:CustomValidator>
            <br />
            <br />

            <fieldset>
                <legend>How would you like to leave your dog with us?</legend>
                <asp:RadioButtonList runat="server" ID="appointmentStartType">
                    <asp:ListItem Text="startAsDropOff">I would like to drop my dog off</asp:ListItem>
                    <asp:ListItem Text="startAsPickUp">I would like for you to pick up my dog from my address</asp:ListItem>
                </asp:RadioButtonList>
                <asp:CustomValidator runat="server" ErrorMessage="Please Select an Option" ForeColor="Red" ControlToValidate="appointmentStartType" OnServerValidate="startTypeValidator" ValidateEmptyText="true"></asp:CustomValidator>
                <br />

                <asp:Label runat="server" AssociatedControlID="appointmentStartTime">What time would you like for pick up/drop off: </asp:Label>
                <asp:DropDownList runat="server" ID="appointmentStartTime">
                    <asp:ListItem Value="12:00:00" Text="12:00pm"></asp:ListItem>
                    <asp:ListItem Value="13:00:00" Text="1:00pm"></asp:ListItem>
                    <asp:ListItem Value="14:00:00" Text="2:00pm"></asp:ListItem>
                    <asp:ListItem Value="15:00:00" Text="3:00pm"></asp:ListItem>
                    <asp:ListItem Value="16:00:00" Text="4:00pm"></asp:ListItem>
                    <asp:ListItem Value="17:00:00" Text="5:00pm"></asp:ListItem>
                    <asp:ListItem Value="18:00:00" Text="6:00pm"></asp:ListItem>
                </asp:DropDownList>
            </fieldset>

            <fieldset>
                <legend>How would you like to take back your dog?</legend>
                <asp:RadioButtonList runat="server" ID="appointmentEndType">
                    <asp:ListItem Text="endAsPickUp">I would like to come pick up my dog</asp:ListItem>
                    <asp:ListItem Text="endAsDropOff">I would like my dog to be dropped off at my address</asp:ListItem>
                </asp:RadioButtonList>
                <asp:CustomValidator runat="server" ErrorMessage="Please Select an Option" ForeColor="Red" ControlToValidate="appointmentEndType" OnServerValidate="endTypeValidator" ValidateEmptyText="true"></asp:CustomValidator>
                <br />

                <asp:Label runat="server" AssociatedControlID="appointmentEndTime">What time would you like for pick up/drop off: </asp:Label>
                <asp:DropDownList runat="server" ID="appointmentEndTime">
                    <asp:ListItem Value="13:00:00" Text="1:00pm"></asp:ListItem>
                    <asp:ListItem Value="14:00:00" Text="2:00pm"></asp:ListItem>
                    <asp:ListItem Value="15:00:00" Text="3:00pm"></asp:ListItem>
                    <asp:ListItem Value="16:00:00" Text="4:00pm"></asp:ListItem>
                    <asp:ListItem Value="17:00:00" Text="5:00pm"></asp:ListItem>
                    <asp:ListItem Value="18:00:00" Text="6:00pm"></asp:ListItem>
                    <asp:ListItem Value="19:00:00" Text="7:00pm"></asp:ListItem>
                    <asp:ListItem Value="20:00:00" Text="8:00pm"></asp:ListItem>
                </asp:DropDownList>
                <asp:CustomValidator runat="server" ErrorMessage="End Time Must Be Later Than Start Time" ForeColor="Red" ControlToValidate="appointmentEndTime" OnServerValidate="endTimeValidator"></asp:CustomValidator>
            </fieldset>
            <br />

            <asp:CheckBox runat="server" ID="clientEmailSubscribe" Text="I would like to receive emails from Local Dog Services" checked="true"/>
            <br />

            <br />
            <asp:Button runat="server" ID="submitButton" OnClick="SubmitBooking" Text="Submit"/>
            <br />
        </div>
    </form>

    <br />
    <div runat="server" id="totalPrice"></div>
    <div runat="server" id="pricingSummary"></div>
    <div runat="server" id="bookingSummary"></div>
</body>
</html>
