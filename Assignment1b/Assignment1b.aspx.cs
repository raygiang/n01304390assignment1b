﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment1b
{
    public partial class Assignment1b : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        // Verifies that if dog walking is selected then the length of the walk must also be specified
        // or if a length of the walk is specified then dog walking must be selected
        protected void appointmentWalkValidator(object source, ServerValidateEventArgs args)
        {
            if ((appointmentWalkLength.Text == "" && !appointmentServices.Items[0].Selected)
                || (appointmentWalkLength.Text != "" && appointmentServices.Items[0].Selected))
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }
        }

        // Verifies that at least one service is selected
        protected void servicesValidator(object source, ServerValidateEventArgs args)
        {
            if (appointmentServices.Items[0].Selected || appointmentServices.Items[1].Selected || appointmentServices.Items[2].Selected)
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }
        }

        // Verifies that an option for recieving the dog is chosen
        protected void startTypeValidator(object source, ServerValidateEventArgs args)
        {
            if (appointmentStartType.SelectedValue != "")
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }
        }

        // Verifies that an option for returning the dog is chosen
        protected void endTypeValidator(object source, ServerValidateEventArgs args)
        {
            if (appointmentEndType.SelectedValue != "")
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }
        }

        // Verifies that the end of the appointment must be after the start of the appointment
        protected void endTimeValidator(object source, ServerValidateEventArgs args)
        {
            if (int.Parse(appointmentEndTime.SelectedValue.Substring(0,2)) > int.Parse(appointmentStartTime.SelectedValue.Substring(0, 2)))
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }
        }

        protected void SubmitBooking(object sender, EventArgs e)
        {
            if(!Page.IsValid)
            {
                return;
            }

            // Creating the Client object
            string name = clientName.Text;
            string phoneNumber = clientPhone.Text;
            string email = clientEmail.Text;
            string streetAddress = clientStreetAddress.Text;
            string postalCode = clientPostalCode.Text;
            bool wantMail = clientEmailSubscribe.Checked;

            Client newClient = new Client(name, phoneNumber, email, streetAddress, postalCode, wantMail);

            // Creating the Dog object
            string dName = dogName.Text;
            string breed = dogBreed.Text;

            Dog newDog = new Dog();
            newDog.DogName = dName;
            newDog.DogBreed = breed;

            // Creating the Appointment object
            List<string> services = new List<string>();
            for (int i=0; i<appointmentServices.Items.Count; i++)
            {
                if (appointmentServices.Items[i].Selected)
                {
                    services.Add(appointmentServices.Items[i].Value);
                }
            }

            int walkLength = 0;
            if(appointmentWalkLength.Text != "")
            {
                walkLength = int.Parse(appointmentWalkLength.Text);
            }

            bool getDog = false;
            if (appointmentStartType.SelectedValue == "I would like for you to pick up my dog from my address")
            {
                getDog = true;
            }

            bool returnDog = false;
            if (appointmentEndType.SelectedValue == "I would like my dog to be dropped off at my address")
            {
                returnDog = true;
            }

            DateTime today = DateTime.Today;
            int startHour = int.Parse(appointmentStartTime.SelectedValue.Substring(0, 2));
            int startMinutes = int.Parse(appointmentStartTime.SelectedValue.Substring(3, 2));
            int startSeconds = int.Parse(appointmentStartTime.SelectedValue.Substring(6, 2));
            int endHour = int.Parse(appointmentEndTime.SelectedValue.Substring(0, 2));
            int endMinutes = int.Parse(appointmentEndTime.SelectedValue.Substring(3, 2));
            int endSeconds = int.Parse(appointmentEndTime.SelectedValue.Substring(6, 2));

            DateTime startDate = new DateTime(today.Year, today.Month, today.Day, startHour, startMinutes, startSeconds);
            DateTime endDate = new DateTime(today.Year, today.Month, today.Day, endHour, endMinutes, endSeconds);

            Appointment newAppointment = new Appointment();
            newAppointment.ServicesRequired = services;
            newAppointment.DogWalkLength = walkLength;
            newAppointment.GetDog = getDog;
            newAppointment.ReturnDog = returnDog;
            newAppointment.ReceiveDate = startDate;
            newAppointment.ReturnDate = endDate;

            // Creating a Booking using our Client, Dog, and Appointment 
            Booking newBooking = new Booking();
            newBooking.Client = newClient;
            newBooking.Dog = newDog;
            newBooking.Appointment = newAppointment;

            // Uses the three methods in the Booking class to display information about the booking
            totalPrice.InnerHtml = "<h2>Total Price: $" + newBooking.CalculatePrice() + "</h2>";
            pricingSummary.InnerHtml = newBooking.GetPricingSummary();
            bookingSummary.InnerHtml = newBooking.GetBookingDetails();
        }
    }
}