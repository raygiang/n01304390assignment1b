﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment1b
{
    public class Appointment
    {
        private List<string> servicesRequired;
        private int dogWalkLength;
        private bool getDog;
        private bool returnDog;
        private DateTime receiveDate;
        private DateTime returnDate;

        public Appointment()
        {

        }

        public List<string> ServicesRequired
        {
            get { return servicesRequired; }
            set { servicesRequired = value;  }
        }

        public int DogWalkLength
        {
            get { return dogWalkLength; }
            set { dogWalkLength = value;  }
        }

        public bool GetDog
        {
            get { return getDog; }
            set { getDog = value; }
        }

        public bool ReturnDog
        {
            get { return returnDog; }
            set { returnDog = value; }
        }

        public DateTime ReceiveDate
        {
            get { return receiveDate; }
            set { receiveDate = value; }
        }

        public DateTime ReturnDate
        {
            get { return returnDate; }
            set { returnDate = value; }
        }
    }
}