﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment1b
{
    public class Dog
    {
        private string dogName;
        private string dogBreed;

        public Dog()
        {
        }

        public string DogName
        {
            get { return dogName; }
            set { dogName = value; }
        }

        public string DogBreed
        {
            get { return dogBreed; }
            set { dogBreed = value; }
        }
    }
}