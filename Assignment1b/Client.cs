﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment1b
{
    public class Client
    {
        public string clientName;
        public string clientPhone;
        public string clientEmail;
        public string clientStreetAddress;
        public string clientPostalCode;
        public bool clientWantsEmail;

        public Client(string name, string phone, string email, string address, string postal, bool wantMail)
        {
            clientName = name;
            clientPhone = phone;
            clientEmail = email;
            clientStreetAddress = address;
            clientPostalCode = postal;
            clientWantsEmail = wantMail;
        }
    }
}